import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CoreRoutingModule } from './core-routing.module';
import { CommonModule } from '@angular/common';
import { RootRouterComponent } from './root-router/root-router.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonItemsModule } from '../common-items/common-items.module';
import { HttpService } from '@services/http.service';
import { HttpInterceptorService } from '@services/http-interceptor.service';
import { URLS } from 'src/app/shared/constants/urls';
import { CoinsDashboardModule } from '../coins-dashboard/coins-dashboard.module';

@NgModule({
  declarations: [RootRouterComponent],
  imports: [
    BrowserModule,
    CoreRoutingModule,
    CommonModule,
    HttpClientModule,
    CommonItemsModule,
    CoinsDashboardModule,
  ],
  providers: [
    HttpService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
    URLS,
  ],
  bootstrap: [RootRouterComponent],
})
export class CoreModule {}
