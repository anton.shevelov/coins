import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoinsAnalyticsComponent } from '../coins-dashboard/coins-analytics/coins-analytics.component';
import { CoinsDashboardModule } from '../coins-dashboard/coins-dashboard.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '@reducers';
import { COINS_ROOT } from '../coins-dashboard/coins-module-routes';

const routes: Routes = [
  {
    path: '',
    redirectTo: COINS_ROOT,
    pathMatch: 'full',
  },
  {
    path: COINS_ROOT,
    component: CoinsAnalyticsComponent,
    loadChildren: () =>
      import('../coins-dashboard/coins-dashboard.module').then(m => m.CoinsDashboardModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), StoreModule.forRoot(reducers)],
  exports: [RouterModule],
})
export class CoreRoutingModule {}
