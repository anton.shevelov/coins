import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { SetError } from '@store/error-action';
import { SomeError } from '@classes/error';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  @Input() message = '';
  constructor(private store: Store<any>) {}

  ngOnInit() {}

  closeError() {
    this.store.dispatch(new SetError(new SomeError()));
  }
}
