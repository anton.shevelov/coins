import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './error/error.component';
import { LoaderComponent } from './loader/loader.component';

const DECLARATION_LIST = [ErrorComponent, LoaderComponent];

@NgModule({
  declarations: [DECLARATION_LIST],
  imports: [CommonModule],
  exports: [DECLARATION_LIST],
})
export class CommonItemsModule {}
