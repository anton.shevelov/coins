import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ICoinHistory } from '@interfaces/coin-history';
import { Observable, Subject } from 'rxjs';
import { HttpService } from '@services/http.service';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { Chart } from 'chart.js';
import * as moment from 'moment';

@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.component.html',
  styleUrls: ['./chart-view.component.scss']
})

export class ChartViewComponent implements OnInit {
  @ViewChild('canvas', { static: true }) canvas: ElementRef;
  public coinsHistory: Observable<ICoinHistory[]>;
  private unsibscribeCoinsHistory: Subject<boolean> = new Subject<boolean>();
  public coinsHistoryBarChart = [];

  public defaultDataSet = {
    data: [],
    borderColor: 'transparent',
    backgroundColor: 'transparent',
  };
  constructor(private http: HttpService, private store: Store<any>) {}

  ngOnInit() {
    this.initChart();
    this.coinsHistory = this.store.select('coinsHistory');
    this.coinsHistory.pipe(takeUntil(this.unsibscribeCoinsHistory)).subscribe(coinsHistory => {
      if (coinsHistory.length) {
        const dataSets = coinsHistory.map(ch => {
          return {
            data: ch.history.map(chh => {
              const item = {
                x: chh.timestamp,
                y: +chh.price,
              };
              return item;
            }),
            borderColor: ch.color,
            backgroundColor: 'transparent',
            showLine: true,
            borderWidth: 0,
            pointRadius: 0,
            pointHoverRadius: 0,
            fill: false,
            tension: 0,
          };
        });
        this.updateChart(dataSets);
      } else {
        this.updateChart(this.defaultDataSet);
      }
    });
  }

  initChart(): void {
    var ctx = this.canvas.nativeElement.getContext('2d');
    this.coinsHistoryBarChart = new Chart(ctx, {
      type: 'scatter',
      data: {
        labels: '',
        datasets: this.defaultDataSet,
      },
      options: {
        legend: {
          display: false,
        },
        scales: {
          xAxes: [
            {
              display: true,
              ticks: {
                callback: (value) => { 
                    return moment(value).format('DD/MMM/YY'); 
                },
            },
            },
          ],
          yAxes: [
            {
              display: true,
            },
          ],
        },
      },
    });
  }

  updateChart(dataSets): void {
    (this.coinsHistoryBarChart as any).data.datasets = dataSets;
    (this.coinsHistoryBarChart as any).update();
  }
  ngOnDestroy(): void {
    this.unsibscribeCoinsHistory.next();
    this.unsibscribeCoinsHistory.complete();
  }

}
