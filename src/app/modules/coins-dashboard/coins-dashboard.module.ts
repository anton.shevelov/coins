import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoinsAnalyticsComponent } from './coins-analytics/coins-analytics.component';
import { CoinsListComponent } from './coins-list/coins-list.component';
import { ChartViewComponent } from './chart-view/chart-view.component';
import { TableViewComponent } from './table-view/table-view.component';
import { PageNavigatorComponent } from './page-navigator/page-navigator.component';
import { CoinsRoutes } from './coins-module-routes';
import { CoinsRoutingModule } from './coins-routing.module';

const EXPORTS = [CoinsAnalyticsComponent];

@NgModule({
  declarations: [
    EXPORTS,
    ChartViewComponent,
    TableViewComponent,
    PageNavigatorComponent,
    CoinsListComponent,
  ],
  imports: [CommonModule, CoinsRoutingModule],
  exports: [EXPORTS],
  providers: [CoinsRoutes],
})
export class CoinsDashboardModule {}
