import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinsAnalyticsComponent } from './coins-analytics.component';

describe('CoinsAnalyticsComponent', () => {
  let component: CoinsAnalyticsComponent;
  let fixture: ComponentFixture<CoinsAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinsAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinsAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
