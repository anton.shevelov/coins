import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HttpService } from '@services/http.service';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-coins-analytics',
  templateUrl: './coins-analytics.component.html',
  styleUrls: ['./coins-analytics.component.scss'],
})
export class CoinsAnalyticsComponent implements OnInit, OnDestroy {
  constructor(private http: HttpService, private store: Store<any>) {}

  ngOnInit() {
    this.http.getCoinsList();
  }

  ngOnDestroy(): void {}
}
