import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ICoin } from '@interfaces/coin';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { SortBy } from '@store/coins-analitics-action';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss'],
})
export class TableViewComponent implements OnInit {
  public moment = moment;
  public coinsAnalitics: Observable<ICoin[]>;

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.coinsAnalitics = this.store.select('coinsAnalitics');
  }

  sortBy(param: string): void {
    this.store.dispatch(new SortBy(param));
  }
}
