export const COINS_ROOT = 'coins';
export const CHART_VIEW = 'coins-chart';
export const TABLE_VIEW = 'coins-table';

/** this class purposed for passing paths into component */
export class CoinsRoutes {
  public CHART_VIEW = `/${COINS_ROOT}/${CHART_VIEW}`;
  public TABLE_VIEW = `/${COINS_ROOT}/${TABLE_VIEW}`;
}
