export class SomeError {
  public message = '';
  public show = false;

  constructor(message: string = '', show: boolean = false) {
    this.message = message;
    this.show = show;
  }
}
