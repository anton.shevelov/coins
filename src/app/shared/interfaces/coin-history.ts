export interface ICoinHistory {
  id: number;
  color: string;
  history: {
    price: string;
    timestamp: number;
  }[];
}
