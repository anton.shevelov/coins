export interface ICoin {
  symbol: string;
  name: string;
  color: string;
  id: number;
  price: string;
  rank: number;
  active: boolean;
}
