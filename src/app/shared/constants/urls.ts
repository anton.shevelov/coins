import { environment } from 'src/environments/environment';

export class URLS {
  public coins = `${environment.SERVER_URL}/coins`;
  public coin = `${environment.SERVER_URL}/coin/`;
  public coinHistory(coin_id: number, timeframe: string = '24h'): string {
    // https://api.coinranking.com/v1/public/coin/1/history/24h
    return `${environment.SERVER_URL}/coin/${coin_id}/history/${timeframe}`;
  }
}
