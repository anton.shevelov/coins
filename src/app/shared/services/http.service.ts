import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { URLS } from '../constants/urls';
import { SetLoader } from '@store/loader-action';
import { SetCoinsList } from '@store/coins-list-action';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  public httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient, private urls: URLS, private store: Store<any>) {}

  defaultGetRequest(url: string): Observable<any> {
    return this.http.get(url, this.httpOptions);
  }
  getCoin(coinID: number): Observable<any> {
    return this.defaultGetRequest(`${this.urls.coin}/${coinID}`);
  }
  getCoinsList(): void {
    this.defaultGetRequest(`${this.urls.coins}`).subscribe(
      successResponce => {
        this.store.dispatch(new SetLoader(false));
        this.store.dispatch(new SetCoinsList(successResponce.data.coins));
      },
      errorResponce => {
        console.log('errorResponce', errorResponce);
      },
    );
  }
  getCoinHistory(coinID: number, timeframe: string = '30d'): Observable<any> {
    return this.defaultGetRequest(`${this.urls.coinHistory(coinID, timeframe)}`);
  }
  getCoinAnalitics(coinID: number): Observable<any> {
    return this.defaultGetRequest(`${this.urls.coin}/${coinID}`);
  }
}
