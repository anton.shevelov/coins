import { defaultCoinsAnalitics } from './coins-analitics-action';
import { CoinsAnaliticsListActions } from '@actions';
import { CoinsAnaliticsListActionType } from '@types';
import { sortBy } from 'lodash';

export function coinsAnaliticsList(
  state = defaultCoinsAnalitics,
  action: CoinsAnaliticsListActionType,
) {
  switch (action.type) {
    case CoinsAnaliticsListActions.PUSH_COINS_ANALITICS_LIST:
      return Object.assign([], state.concat([action.payload] as any));
    case CoinsAnaliticsListActions.REMOVE_FROM_COINS_ANALITICS_LIST:
      return Object.assign(
        [],
        state.filter(coinAnaliticsItem => {
          return coinAnaliticsItem.id !== action.payload;
        }),
      );
    case CoinsAnaliticsListActions.SORT_BY:
      return Object.assign([], sortBy(state, [action.payload]));
    default:
      return state;
  }
}
