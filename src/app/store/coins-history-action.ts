import { Action } from '@ngrx/store';
import { ICoinHistory } from '@interfaces/coin-history';
import { CoinsHistoryListActions } from '@actions';

export const defaultCoinsHistoryList: ICoinHistory[] = [];

export class PushToCoinsHistoryList implements Action {
  readonly type = CoinsHistoryListActions.PUSH_COINS_HISTORY_LIST;
  constructor(public payload: ICoinHistory) {}
}
export class RemoveFromHistoryList implements Action {
  readonly type = CoinsHistoryListActions.REMOVE_FROM_COINS_HISTORY_LIST;
  constructor(public payload: number) {}
}
