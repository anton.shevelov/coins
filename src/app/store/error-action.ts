import { Action } from '@ngrx/store';
import { ErrorActions } from '@actions';
import { SomeError } from '@classes/error';

export const defaultErrorState = new SomeError();

export class SetError implements Action {
  readonly type = ErrorActions.ERROR_STATE;
  constructor(public payload: SomeError) {}
}
