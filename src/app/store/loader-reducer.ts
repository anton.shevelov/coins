import { defaultLoaderState } from './loader-action';
import { LoaderStateActionType } from '@types';
import { LoaderActions } from '@actions';

export function showLoader(state = defaultLoaderState, action: LoaderStateActionType) {
  switch (action.type) {
    case LoaderActions.LOADER_STATE:
      return action.payload;
    default:
      return state;
  }
}
