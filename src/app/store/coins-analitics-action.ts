import { Action } from '@ngrx/store';
import { CoinsAnaliticsListActions } from '@actions';
import { ICoinAnalitics } from '@interfaces/coin-analitics';

export const defaultCoinsAnalitics: ICoinAnalitics[] = [];

export class PushToCoinsAnaliticsList implements Action {
  readonly type = CoinsAnaliticsListActions.PUSH_COINS_ANALITICS_LIST;
  constructor(public payload: ICoinAnalitics) {}
}
export class RemoveFromAnaliticsList implements Action {
  readonly type = CoinsAnaliticsListActions.REMOVE_FROM_COINS_ANALITICS_LIST;
  constructor(public payload: number) {}
}

export class SortBy implements Action {
  readonly type = CoinsAnaliticsListActions.SORT_BY;
  constructor(public payload: string) {}
}

