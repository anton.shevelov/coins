import { CoinStateActionType } from '@types';
import { CoinListActions } from '@actions';
import { defaultCoinList } from './coins-list-action';

export function coinsList(state = defaultCoinList, action: CoinStateActionType) {
  switch (action.type) {
    case CoinListActions.COINS_LIST:
      return Object.assign([], action.payload);
    case CoinListActions.COINS_STATE:
      state[action.payload].active = !state[action.payload].active;
      return Object.assign([], state);
    default:
      return state;
  }
}
