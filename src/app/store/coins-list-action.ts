import { Action } from '@ngrx/store';
import { CoinListActions } from '@actions';
import { ICoin } from '@interfaces/coin';

export const defaultCoinList: ICoin[] = [];

export class SetCoinsList implements Action {
  readonly type = CoinListActions.COINS_LIST;
  constructor(public payload: ICoin[]) {}
}
export class UpdateCoinState implements Action {
  readonly type = CoinListActions.COINS_STATE;
  constructor(public payload: number) {}
}
